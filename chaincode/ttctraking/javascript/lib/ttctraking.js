/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const LimiteTemperatura = -7;

class Ttctraking extends Contract {

    async initLedger(ctx) {
        console.info('============= TTC - Início : Inicializando Ledger ===========');
        const lotesBacon = [
            {
                numeroLote: "121212",
                nome: "Carne da Boa",
                alerta: "",
                dadosAlteraveis: [
                    {
                        iteracao: 1,
                        temperatura: -12,
                        peso: 103.4,
                        detentorDaCarga: "Matadouro de Zé",
                        alerta: ""
                    },
                ]
            },
        ];

        lotesBacon[0].docType = 'bacon';
        await ctx.stub.putState("121212", Buffer.from(JSON.stringify(lotesBacon[0])));
        console.info('Adicionado <--> ', lotesBacon[0]);
        console.info('============= TTC - Fim : Inicializando Ledger ===========');
    }

    /**
     * Retorna o lote selecionado no bloco atual da rede
     * Retorna apenas a última alteração feita no bloco
     * @param {*} ctx - Smart Contract passado pelo SDK do HyperLedger Fabric 
     * @param {*} numeroLote - Numero do Lote a ser consultado
     */
    async queryLote(ctx, numeroLote) {
        // Recupera o lote apartir do numero de lote passado
        const streamBloco = await ctx.stub.getState(numeroLote); 
        if (!streamBloco || streamBloco.length === 0) {
            throw new Error(`O lote: ${numeroLote} não foi encontrado.`);
        }
        let loteDeBacon = JSON.parse(streamBloco.toString());
        //Restringe o retorno do bloco a apenas a última alteracao no bloco.
        loteDeBacon.dadosAlteraveis = loteDeBacon.dadosAlteraveis[loteDeBacon.dadosAlteraveis.length-1];
        return JSON.stringify(loteDeBacon);
    }

    /**
     * Retorna apenas o histórico de alerações do lote selecionado
     * @param {*} ctx - Smart Contract passado pelo SDK do HyperLedger Fabric 
     * @param {*} numeroLote - Numero do Lote a ser consultado
     */
    async queryHistoricoLote(ctx, numeroLote) {

        // Recupera o lote apartir do numero de lote passado
        const streamBloco = await ctx.stub.getState(numeroLote); 
        if (!streamBloco || streamBloco.length === 0) {
            throw new Error(`O lote: ${numeroLote} não foi encontrado.`);
        }
        let loteDeBacon = JSON.parse(streamBloco.toString());
        return JSON.stringify(loteDeBacon.dadosAlteraveis);
    }

    /**
     * Retorna todas as informações de todos os lotes armazenados no bloco
     * @param {*} ctx 
     */
    async queryTodosLotes(ctx) {
        //Ao deixar as duas chave vazias a rede retorna o world state completo.
        const startKey = '';
        const endKey = '';
        ctx.stub.get
        const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString('utf8'));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString('utf8');
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async changeInfo(ctx, numeroLote, detentorDaCarga, peso = '', temperatura = '') {
        console.info(`Iniciando alteração de propriedade do lote`);
        const streamBloco = await ctx.stub.getState(numeroLote); 
        if (!streamBloco || streamBloco.length === 0) {
            throw new Error(`O lote: ${numeroLote} não foi encontrado.`);
        }
        
        let loteDeBacon = JSON.parse(streamBloco.toString());
        console.info('Bloco: ' + loteDeBacon)
        let dadosUltimaIteracao = loteDeBacon.dadosAlteraveis[loteDeBacon.dadosAlteraveis.length-1];
        console.info('Dados para alteracao: ' + dadosUltimaIteracao)

        //Caso a temperatura esteja acima da temperatura limite, emite um alerta no bloco e na iteracao.
        let alerta = ""
        if(temperatura != '' && temperatura > LimiteTemperatura) {
            alerta = "Temperatura ultrapassou o limite permitido";
            loteDeBacon.alerta = alerta;
        }

        const dadosParaAlteracao = {
                    //Adiciona 1 na iteração
                    iteracao: dadosUltimaIteracao.iteracao + 1,
                    //Caso tenha sido passada temperatura, atualiza com a nova, caso contrário mantém a antiga
                    temperatura: temperatura == '' ? dadosUltimaIteracao.temperatura : temperatura,
                    //Caso tenha sido passado o peso, atualiza com o novo, caso contrário mantém o antigo
                    peso: peso == '' ? dadosUltimaIteracao.peso : peso,
                    //Atualiza o detentor da carga
                    detentorDaCarga: detentorDaCarga,
                    //Atualiza o alerta
                    alerta: alerta
                };
        
        //Atualiza o atributo selenado pelo usuario
        console.info('Dados alterados: ' + dadosParaAlteracao)
        loteDeBacon.dadosAlteraveis.push(dadosParaAlteracao);
        console.info('Bloco alterado: ' + loteDeBacon)
        await ctx.stub.putState(numeroLote, Buffer.from(JSON.stringify(loteDeBacon)));
    }
}

module.exports = Ttctraking;