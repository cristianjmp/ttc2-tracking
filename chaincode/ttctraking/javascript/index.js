/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const Ttctraking = require('./lib/ttctraking');

module.exports.Ttctraking = Ttctraking;
module.exports.contracts = [ Ttctraking ];