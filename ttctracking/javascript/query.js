/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { FileSystemWallet, Gateway } = require('fabric-network');
const path = require('path');

const ccpPath = path.resolve(__dirname, '..', '..', 'first-network', 'connection-org1.json');
const wallet = generateWallet();

async function main() {
    try {
        const channel = 'mychannel';

        console.log("Usuário abatedouro");
        await executaExemploAbatedouro('abatedouro', channel);
        console.log("Usuário transportador1");
        await executaExemploTransportador1('transportador1', channel);
        console.log("Usuário industria");
        await executaExemploIndustria('industria', channel);
        console.log("Usuário transportador2");
        await executaExemploTransportador2('transportador2', channel);
        console.log("Usuário supermercado");
        await executaExemploSupermercado('supermercado', channel);
        console.log("Usuário cliente-final");
        await executaExemploClienteFinal('cliente-final', channel);
        console.log("Usuário fiscalizacao");
        await executaExemploFiscalizacao('fiscalizacao', channel);

    } catch (error) {
        console.error(`Falha ao recuperar a transação \n: ${error}`);
        process.exit(1);
    }
}

function generateWallet() {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), 'wallet');
    return new FileSystemWallet(walletPath);
}

async function checkUserInWallet(user) {
    const userExists = await wallet.exists(user);
    if (!userExists) {
        let msg = `A identidade do usuário "${user}" não foi encontrada na wallet \n Execute o programa registerUser.js para criar suas credenciais`;
        throw msg;
    }
}

async function connectToChannel(user, channel) {
    let gateway = new Gateway();
    // Create a new gateway for connecting to our peer node.
    await gateway.connect(ccpPath, { wallet, identity: user, discovery: { enabled: true, asLocalhost: true } });
    // Get the network (channel) our contract is deployed to.
    return await gateway.getNetwork(channel);
}

async function getInfoLote(contract, numeroLote) {
    let dadosLote = await contract.evaluateTransaction('queryLote', numeroLote);
    return JSON.parse(dadosLote.toString('utf8'));
}

function exibeDadosLote(lote) {
    console.log(`\tDados do Lote
            Número: ${lote.numeroLote}
            Nome do produto: ${lote.nome}
            Alerta do lote: ${lote.alerta}
            Temperatura atual: ${lote.dadosAlteraveis.temperatura}
            Peso atual: ${lote.dadosAlteraveis.peso}
            Detentor atual da carga: ${lote.dadosAlteraveis.detentorDaCarga}
            Alerta da iteração: ${lote.dadosAlteraveis.alerta}
    `);
}

async function getHistoricoLote(contract, numeroLote) {
    let historicoLote = await contract.evaluateTransaction('queryHistoricoLote', numeroLote);
    return JSON.parse(historicoLote.toString('utf8'));
}

function exibeDadosHistoricoLote(historicoLote, numeroLote) {
    if(historicoLote == null || historicoLote.length <= 0) {
        console.log("Não foram feitar alterações neste lote.");
        return;
    }
    console.log(`\tHitórico do Lote ${numeroLote}`);
    historicoLote.forEach(alteracaoLote => {
        console.log(`\tIteração ${alteracaoLote.iteracao} 
            Temperatura Peso
            ${alteracaoLote.temperatura} \t\t ${alteracaoLote.peso} 
            Detentor: ${alteracaoLote.detentorDaCarga} 
            Alerta: ${alteracaoLote.alerta}`);
    });
}

async function executaAlteraCoesNoLote(contract, numeroLote, detentorDaCarga, peso, temperatura) {
    console.log("Iniciando processo de alterações no lote " + numeroLote);
    await contract.submitTransaction('changeInfo', numeroLote, detentorDaCarga, peso, temperatura);
    let lote = await getInfoLote(contract, numeroLote);
    exibeDadosLote(lote);
}

async function executaExemploAbatedouro(usuario, channel) {
    let numeroLote = '121212';

    //Cria uma conexão para o usuario selecionado no canal definido
    let network = await connectToChannel(usuario, channel);
    
    // Get the contract from the network.
    let contract = network.getContract('ttctraking');
    await executaAlteraCoesNoLote(contract, numeroLote, 'Abatedouro', '103.4', "-16");
    
    let historicoLote = await getHistoricoLote(contract, numeroLote);
    exibeDadosHistoricoLote(historicoLote, numeroLote);
}

async function executaExemploTransportador1(usuario, channel) {
    let numeroLote = '121212';
    
    //Cria uma conexão para o usuario selecionado no canal definido
    let network = await connectToChannel(usuario, channel);
    
    // Get the contract from the network.
    let contract = network.getContract('ttctraking');        
    await executaAlteraCoesNoLote(contract, numeroLote, 'Transportadora 1', '', "-3");
    
    let historicoLote = await getHistoricoLote(contract, numeroLote);
    exibeDadosHistoricoLote(historicoLote, numeroLote);
}

async function executaExemploIndustria(usuario, channel) {
    let numeroLote = '121212';

    //Cria uma conexão para o usuario selecionado no canal definido
    let network = await connectToChannel(usuario, channel);
    
    // Get the contract from the network.
    let contract = network.getContract('ttctraking');        
    await executaAlteraCoesNoLote(contract, numeroLote, 'Indústria processadora de bacon', '', "-12");
    
    let historicoLote = await getHistoricoLote(contract, numeroLote);
    exibeDadosHistoricoLote(historicoLote, numeroLote);
}

async function executaExemploTransportador2(usuario, channel) {
    let numeroLote = '121212';

    //Cria uma conexão para o usuario selecionado no canal definido
    let network = await connectToChannel(usuario, channel);

    // Get the contract from the network.
    let contract = network.getContract('ttctraking');

    await executaAlteraCoesNoLote(contract, numeroLote, 'Transportadora 2', '', "-8");
    let historicoLote = await getHistoricoLote(contract, numeroLote);
    exibeDadosHistoricoLote(historicoLote, numeroLote);
}

async function executaExemploSupermercado(usuario, channel) {
    let numeroLote = '121212';
    
    //Cria uma conexão para o usuario selecionado no canal definido
    let network = await connectToChannel(usuario, channel);
    
    // Get the contract from the network.
    let contract = network.getContract('ttctraking');        
    await executaAlteraCoesNoLote(contract, numeroLote, 'Supermercado', '', "-10");
    
    let historicoLote = await getHistoricoLote(contract, numeroLote);
    exibeDadosHistoricoLote(historicoLote, numeroLote);
}

async function executaExemploClienteFinal(usuario, channel) {
    let numeroLote = '121212';

    //Cria uma conexão para o usuario selecionado no canal definido
    let network = await connectToChannel(usuario, channel);
    
    // Get the contract from the network.
    let contract = network.getContract('ttctraking');
    await executaAlteraCoesNoLote(contract, numeroLote, 'Cliente Final', '', "4");
    
    let historicoLote = await getHistoricoLote(contract, numeroLote);
    exibeDadosHistoricoLote(historicoLote, numeroLote);
}

async function executaExemploFiscalizacao(usuario, channel) {
    let numeroLote = '121212';
    //Cria uma conexão para o usuario selecionado no canal definido
    let network = await connectToChannel(usuario, channel);

    // Get the contract from the network.
    let contract = network.getContract('ttctraking');

    let historicoLote = await getHistoricoLote(contract, numeroLote);
    console.log(`Usuário de fiscalização ${usuario} verificando histórico do lote:`)
    exibeDadosHistoricoLote(historicoLote, numeroLote);
}
main();